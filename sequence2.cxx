// FILE: sequence2.cxx
// CLASS IMPLEMENTED: sequence (see sequence2.h for documentation)
//1. The number of items in the sequence is stored in the member variable
//used.
//2. The actual items of the bag are stored in a
//partially filled array. The array is a dynamic
//array, pointed to by the member variable data.
//3. The total size of the dynamic array is in the
//member variable capacity.

#include <iostream>
#include <cstdlib>
#include <algorithm> //provides copy
#include <cassert> // Provides assert function
#include "sequence2.h"

using namespace std;
namespace main_savitch_4
{
    sequence::sequence(size_type initial_capacity)
    {
        data = new value_type[initial_capacity];
        capacity = initial_capacity;
        used = 0;
        current_index = 0;
    }

	sequence::sequence(const sequence& source)
	{
		
        data = new value_type[source.capacity];
        capacity = source.capacity;
        used = source.used;
        copy(source.data, source.data + used, data);
        current_index = source.current_index;
        
        
	}

	sequence::~sequence()
	{
		delete[] data;
	}

    void sequence::resize(size_type new_capacity)
    {
    	value_type *larger_array;
    	if(new_capacity == capacity)
    		return;
    	if(new_capacity < used)
    		new_capacity=used;
        larger_array = new value_type[new_capacity];
        copy(data, data + used, larger_array);
        delete [] data;
        data = larger_array;
        capacity = new_capacity; 
    	
    }

	void sequence::start ( )
	{
        current_index = 0;
	}

        sequence::size_type sequence::size( ) const
        {
        		return used;
        }

        bool sequence::is_item( ) const
        {
            return used != 0 && current_index >= 0 && current_index <= used-1;
        }


        sequence::value_type sequence::current( ) const
        {
        	assert(is_item());
                return data[current_index];
        }

    void sequence::advance( )
    {
    	if(is_item()) {
            if (current_index == used - 1)
                current_index = used;
            else
                current_index++;
        }
    }


    void sequence::insert(const value_type& entry)
    {
    	if (used == capacity)
    		resize(used+1);
    	if(size( ) < capacity) {
            size_type i;
            if (current_index >= used)
              current_index = 0;
            for (i = used; i > current_index; --i)
                data[i] = data[i-1];
            data[current_index] = entry;
            used++;
        }
    }

    void sequence::attach(const value_type& entry)
    {
    	if (used == capacity)
    		resize(used+1);
    	if(size( ) < capacity) {
            if (used == 0) {
                data[current_index] = entry;
            } else if(current_index >= used){
                current_index = used;
                data[current_index] = entry;
            }else{
                for (size_t j = used; j > current_index + 1; --j)
                    data[j] = data[j-1];

                data[current_index+1] = entry;
                current_index++;
            }
            used++;
        }

    }

    void sequence::operator=(const sequence& source)
    {
    	value_type *new_data;
    	if(this == &source)
	    	return;
	    if(capacity != source.capacity)
	    {
	    	new_data = new value_type[source.capacity];
	    	delete [] data;
	    	data = new_data;
	    	capacity = source.capacity;
            
	    }
        current_index = source.current_index;
	    used = source.used;
	    copy(source.data, source.data + used, data);
    }

    void sequence::remove_current( )
    {
    	assert(is_item());
            
        if (current_index == used - 1)
        {
            current_index = used - 1;
            used--;
        }
        else {
            for (size_type i = current_index; i < used; i++)
                data[i] = data[i+1];
             used--;
            }
    }

    sequence::value_type sequence::operator [] (size_type index) const
    {
        assert(index >= 0 && index < used);
        return data[index];
    }

    void sequence::operator += (const sequence& addend)
    {
        if(used + addend.used > capacity)
            resize(used + addend.used);
        copy(addend.data, addend.data + addend.used, data + used);
            used += addend.used;
    }

    sequence operator + (const sequence& b1, const sequence& b2)
    {
        sequence answer(b1.size() + b2.size());
        answer += b1;
        answer += b2;
        return answer;
    }

}